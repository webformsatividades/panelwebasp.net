﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace panelASP.NET
{
    public partial class panel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                pnlTela2.Visible = true;
                pnlTela3.Visible = false;
                pnlTela4.Visible = false;
            }
        }

        protected void btnProximoTela2_Click(object sender, EventArgs e)
        {
            pnlTela2.Visible = true;
            pnlTela3.Visible = true;
            pnlTela4.Visible = false;
        }

        protected void btnProximoTela3_Click(object sender, EventArgs e)
        {
            pnlTela2.Visible = true;
            pnlTela3.Visible = true;
            pnlTela4.Visible = true;
        }

        protected void btnVoltarTela3_Click(object sender, EventArgs e)
        {

            pnlTela2.Visible = true;
            pnlTela3.Visible = false;
            pnlTela4.Visible = false;
        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtNome.Text) ||
                string.IsNullOrEmpty(txtSobrenome.Text) ||
                string.IsNullOrEmpty(txtGenero.Text) ||
                string.IsNullOrEmpty(txtCelular.Text) ||
                string.IsNullOrEmpty(txtEndereco.Text) ||
                string.IsNullOrEmpty(txtCEP.Text) ||
                string.IsNullOrEmpty(txtCidade.Text) ||
                string.IsNullOrEmpty(txtUsuario.Text) ||
                string.IsNullOrEmpty(txtSenha.Text))

            {
                lblEnviado.Text = "Por favor, preencha todos os campos.";
                return;
            }

            lblEnviado.Text = "Os dados foram enviados com sucesso!";
            pnlTela2.Visible = true;
            pnlTela3.Visible = true;
            pnlTela4.Visible = true;
        }

        protected void btnVoltarTela4_Click(object sender, EventArgs e)
        {
            pnlTela2.Visible = true;
            pnlTela3.Visible = true;
            pnlTela4.Visible = false;
        }
    }
}