﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="panel.aspx.cs" Inherits="panelASP.NET.panel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Panel</title>
</head>
<style>
    .auto-style1 {
        width: 599px;
        height: 23px;
    }

    .auto-style2 {
        width: 50px;
        height: 53px;
    }

    .auto-style3 {
        z-index: 100;
        width: 680px;
        position: absolute;
        margin-top: 7px;
        margin-left: 7px;
        border-color: lightgreen;
        border-style: solid;
        border-width: 5px;
        left: 10px;
        top: 15px;
        height: 540px;
    }

    .auto-style5 {
        height: 23px;
        text-decoration:underline; 
        font-size: 14pt;
        text-align: center;
    }
</style>
<body>
    <form id="form1" runat="server">
        <table class="auto-style3">
            <tr>
                <td style="text-align: left; text-decoration:underline; font-size: 18pt;" class="auto-style1"><strong>PAINEL EM ASP.NET</strong></td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Panel ID="pnlTela1" runat="server" BorderColor="Maroon" BorderStyle="Solid" Height="570px" Width="315px" BorderWidth="4px">
                        <asp:Panel ID="pnlTela2" runat="server" BorderColor="Black" BorderWidth="4px" BorderStyle="Solid" Width="310px">
                            <table style="width: 300px; height: 25px;">
                                <tr>
                                    <td colspan="2" class="auto-style5"><strong>Informações Pessoais</strong></td>
                                </tr>
                                <tr>
                                    <td>Nome:</td>
                                    <td style="text-align: right">
                                        <asp:TextBox ID="txtNome" runat="server" Height="25px" Width="150px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Sobrenome:</td>
                                    <td style="text-align: right">
                                        <asp:TextBox ID="txtSobrenome" runat="server" Height="25px" Width="150px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Gênero:</td>
                                    <td style="text-align: right">
                                        <asp:TextBox ID="txtGenero" runat="server" Height="25px" Width="150px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Celular:</td>
                                    <td style="text-align: right">
                                        <asp:TextBox ID="txtCelular" runat="server" Height="25px" Width="150px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td style="text-align: center;">
                                        <asp:Button ID="btnProximoTela2" runat="server" Text="Próximo" OnClick="btnProximoTela2_Click" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="pnlTela3" runat="server" BorderColor="#FFCC66" BorderStyle="Solid" BorderWidth="4px" Width="310px">
                            <table style="width: 300px; height: 25px;">
                                <tr>
                                    <td colspan="2" class="auto-style5"><strong>Detalhes de endereço</strong></td>
                                </tr>
                                <tr>
                                    <td>Endereço:
                                    </td>
                                    <td style="text-align: right">
                                        <asp:TextBox ID="txtEndereco" runat="server" Height="25px" Width="150px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Cidade:</td>
                                    <td style="text-align: right">
                                        <asp:TextBox ID="txtCidade" runat="server" Height="25px" Width="150px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>CEP:</td>
                                    <td style="text-align: right">
                                        <asp:TextBox ID="txtCEP" runat="server" Height="25px" Width="150px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td style="text-align: center;">
                                        <asp:Button ID="btnProximoTela3" runat="server" Text="Próximo" OnClick="btnProximoTela3_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td style="text-align: center;">
                                        <asp:Button ID="btnVoltarTela3" runat="server" Text="Voltar" OnClick="btnVoltarTela3_Click" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="pnlTela4" runat="server" BorderColor="#3399FF" BorderStyle="Solid" BorderWidth="4px" Width="310px">
                            <table style="width: 300px; height: 25px;">
                                <tr>
                                    <td colspan="2" class="auto-style5"><strong>Área de logín</strong></td>
                                </tr>
                                <tr>
                                    <td>Usuário:</td>
                                    <td style="text-align: right">
                                        <asp:TextBox ID="txtUsuario" runat="server" Height="25px" Width="150px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Senha:</td>
                                    <td style="text-align: right">
                                        <asp:TextBox ID="txtSenha" runat="server" Height="25px" Width="150px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td style="text-align: center;">
                                        <asp:Button ID="btnEnviar" runat="server" Text="Enviar" OnClick="btnEnviar_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td style="text-align: center;">
                                        <asp:Button ID="btnVoltarTela4" runat="server" Text="Voltar" OnClick="btnVoltarTela4_Click" />
                                    </td>
                                </tr>
                            </table>
                            <asp:Label ID="lblEnviado" runat="server"></asp:Label>
                        </asp:Panel>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
